public class Hash <Key,Value> {
    private int M = 5; // กำหนดให้ M เท่ากับ 5
    private Value[] vals = (Value[]) new Object[M];
    private Key[] keys = (Key[]) new Object[M];


    private int hash(Key key) { // ความยาว hash
		return (key.hashCode()%M);
	}

    public void put (Key key, Value val) { // put
        int i;
        for(i = hash(key); keys[i] != null; i=(i+1) % M)
            if(keys[i].equals(key))
                break;
        keys[i] = key;
        vals[i] = val;
    }

    public Value get(Key key) { // ดึงค่า key

        for (int i=hash(key); keys[i] != null;i =(i+i)%M)
            if(key.equals (keys[i]))
                return vals[i]; // คืนส่งค่า value

        return null; // ถ้าไม่มีค่า key ให้ส่งค่า null
    }
    
    public Value delete(Key key){ // delete
        for (int i=hash(key); keys[i] != null;i =(i+i)%M)
            if(key.equals (keys[i]))
                keys[i] = null;

        return null;
    }




	

}
