
public class Main_Hash {
    public static void main(String[] args) {
        Hash<Integer,String> hash = new Hash<>();

        System.out.println("Add Key and Value in Hash");
        hash.put(10, "Thidarat"); // put
        hash.put(12, "Kodpat");// put
        System.out.println("------------------------");

        System.out.println("Get Value in Hash By Key");
        System.out.println(hash.get(10));
        System.out.println("------------------------");

        System.out.println("Delete Value in Hash By Key");
        hash.delete(10); // delete
        System.out.println(hash.get(10));




     
    }
}
